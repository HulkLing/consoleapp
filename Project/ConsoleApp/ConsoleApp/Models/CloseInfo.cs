﻿using ConsoleApp.Interfaces;
using System;

namespace ConsoleApp.Models
{
    [Serializable]
    public class CloseInfo : IEntity
    {
        public int Id { get; set; }
        public DateTime CloseDate { get; set; }
    }
}
