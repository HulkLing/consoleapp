﻿using ConsoleApp.Interfaces;
using System;

namespace ConsoleApp.Models
{
    [Serializable]
    public class Entrepreneur : IEntityData
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int Profit { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
