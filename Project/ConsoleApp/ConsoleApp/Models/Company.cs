﻿using ConsoleApp.Interfaces;
using System;

namespace ConsoleApp.Models
{
    [Serializable]
    public class Company : IEntityData
    {
        public int Id { get; set; }
        public string Name { get; set;}
        public int Profit { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
