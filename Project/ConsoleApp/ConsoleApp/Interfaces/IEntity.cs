﻿
namespace ConsoleApp.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }

}
