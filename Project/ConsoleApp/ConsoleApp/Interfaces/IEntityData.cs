﻿using System;

namespace ConsoleApp.Interfaces
{
    public interface IEntityData : IEntity
    {
        int Profit { get; set; }
        DateTime CreationDate { get; set; }
        DateTime ReportDate { get; set; }
    }
}
