﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace ConsoleApp
{
    public class Context
    {
        public List<T> XmlFileReader<T>(string fileName, string rootAttributeName)
        {
            List<T> result;
            using (StreamReader fs = new StreamReader("AppData\\" + fileName + ".xml", true))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<T>),
                    new XmlRootAttribute(rootAttributeName));
                result = (List<T>)formatter.Deserialize(fs);
            }
            return result;
        }
    }

}
