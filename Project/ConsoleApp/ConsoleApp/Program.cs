﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApp.Models;

namespace ConsoleApp
{
    class Program
    {
        static void Main()
        {
            Context context = new Context();

            List<Company> companies = context.XmlFileReader<Company>("companies", "Companies").OrderByDescending(x => x.ReportDate)
                .GroupBy(customer => customer.Id).Select(group => group.First()).ToList();
            List<Entrepreneur> entrepreneurs = context.XmlFileReader<Entrepreneur>("entrepreneurs", "Entrepreneurs").OrderByDescending(x => x.ReportDate)
                .GroupBy(customer => customer.Id).Select(group => group.First()).ToList();
            List<CloseInfo> closeInfos = context.XmlFileReader<CloseInfo>("closeinfo", "CloseInfoItems").ToList();

            IEnumerable<Company> companiesContains = companies.Where(t => !closeInfos.Select(fc => fc.Id).Contains(t.Id));
            IEnumerable<Entrepreneur> entrepreneursContains = entrepreneurs.Where(t => !closeInfos.Select(fc => fc.Id).Contains(t.Id));

            var companiesNoContains = companies.Join(closeInfos, p => p.Id, c => c.Id,
                (p, c) => new { Create = p.CreationDate, Close = c.CloseDate, p.Profit});
            var entrepreneursNoContains = entrepreneurs.Join(closeInfos, p => p.Id, c => c.Id,
                (p, c) => new { Create = p.CreationDate, Close = c.CloseDate, p.Profit });

            int activeCompaniesCount = companiesContains.Count();
            int activeEntrepreneurCount = entrepreneursContains.Count();
            int closeCompanyCount = companies.Count()- activeCompaniesCount;
            int closeEntrepreneurCount = entrepreneurs.Count()-activeEntrepreneurCount;

            int doing;

            do
            {
                Console.Clear();
                Console.WriteLine("Количество действующих компаний : {0}", activeCompaniesCount);
                Console.WriteLine("Количество действующих ИП : {0}", activeEntrepreneurCount);
                Console.WriteLine("Количество закрытых компаний : {0}",closeCompanyCount);
                Console.WriteLine("Количество закрытых ИП : {0}",closeEntrepreneurCount);

                Console.WriteLine("Средняя прибыль действующих компаний и ИП :");
                Console.WriteLine("         Компаний : {0:F2}", companiesContains.Average(p => p.Profit));
                Console.WriteLine("         ИП : {0:F2}", entrepreneursContains.Average(p => p.Profit));

                Console.WriteLine("Средняя прибыль недействующих компаний и ИП : ");
                Console.WriteLine("         Компаний : {0:F2}", companiesNoContains.Average(p => p.Profit));
                Console.WriteLine("         ИП : {0:F2}", entrepreneursNoContains.Average(p => p.Profit));

                Console.WriteLine("Среднее время существования компаний и ИП в днях :");
                Console.WriteLine("         Компаний : {0} дней", Math.Ceiling(companiesNoContains.Average(p => p.Close.Subtract(p.Create).Days)));
                Console.WriteLine("         ИП : {0}", Math.Ceiling(entrepreneursNoContains.Average(p => p.Close.Subtract(p.Create).Days)));

                Console.WriteLine("{0}", "0. Выход");
                Console.Write("Действие: ");

                if (!int.TryParse(Console.ReadLine(), out doing))
                {
                    Console.WriteLine("Нужно вводить только числа!");
                    Console.WriteLine("Нажмите любую клавишу для продолжения...");
                    Console.ReadKey();
                    doing = -1;
                }
                else
                    switch (doing)
                    {
                        case 0: return;
                        default:
                            Console.WriteLine("Выберите верный пункт!");
                            Console.WriteLine("Нажмите любую клавишу для продолжения...");
                            Console.ReadKey();
                            break;
                    }

            } while (doing != 0);
        }
    }
}
